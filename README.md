# GitLab Predefined Environment Variables Test

A test of [GitLab predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html). primarily used to test actual values for the [gitlab-ci-env](https://gitlab.com/gitlab-ci-utils/gitlab-ci-env/) project.
